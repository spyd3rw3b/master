def mthly_pay(princ, ann_int, dur):
    amt = 0

    def mthly_rate(princ, ann_int, dur):
        if ann_int != 0:
            rate = (ann_int/100)/12
            return rate
        else:
            return 0

    r = mthly_rate(princ, ann_int, dur)
    n = dur*12

    if (r == 0):
       amt = (princ / (n))
       return amt
    else:
        amt = princ * (r*((1+r)**n)) / (((1+r)**n)-1)
        return amt                